#!/bin/env bash
# Location: $HOME/.local/install/makemkv.sh
# Dependency: 
# Description: add gpg key, makemkv repository, and install makemkv.
# https://forum.makemkv.com/forum/viewtopic.php?f=3&t=24328
# Usage: sudo makemkv.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    curl -L --url https://apt.calenhad.com/debian/calenhad.gpg --output /usr/share/keyrings/calenhad.gpg
    printf "deb [signed-by=/usr/share/keyrings/calenhad.gpg] https://apt.calenhad.com/debian bullseye contrib non-free" > /etc/apt/sources.list.d/makemkv.list
    apt update
    apt --yes install makemkv
fi
