#!/bin/env bash
# Location: $HOME/.local/install/fastflix.sh
# Dependency:
# Description: Install fastflix binary and ffmpeg binary to ~/.local/bin/
# and nvencc deb package.
# Usage: fastflix.sh

if [[ $EUID -gt 0 ]]; then
    
    # fastflix
    rm -fr ~/.local/bin/fastflix
    rm -fr /tmp/ff
    mkdir /tmp/ff
    cd /tmp/ff
    curl -LO https://github.com/cdgriffith/FastFlix/releases/download/4.10.0/FastFlix_4.10.0_ubuntu_20_x86_64.zip
    unzip FastFlix_*
    chmod +x FastFlix
    cp FastFlix ~/.local/bin/fastflix
    
    # nvenc
    sudo apt -y remove nvencc
    curl -LO https://github.com/rigaya/NVEnc/releases/download/6.06/nvencc_6.06_Ubuntu20.04_amd64.deb
    sudo dpkg -i nvencc*

    # ffmpeg
    rm -fr ~/.local/bin/ffmpeg
    rm -fr ~/.local/bin/ffmplay
    rm -fr ~/.local/bin/ffmprobe
    curl -LO https://github.com/BtbN/FFmpeg-Builds/releases/download/latest/ffmpeg-master-latest-linux64-gpl.tar.xz
    tar -xf ffmpeg-master-latest-linux64-gpl.tar.xz
    cp ffmpeg-master-latest-linux64-gpl/bin/ffmpeg ~/.local/bin
    cp ffmpeg-master-latest-linux64-gpl/bin/ffplay ~/.local/bin
    cp ffmpeg-master-latest-linux64-gpl/bin/ffprobe ~/.local/bin

    # hdr10plus_tool
    rm -fr ~/.local/bin/hdr10plus_tool
    rm -fr /tmp/hdr10
    mkdir /tmp/hdr10
    cd /tmp/hdr10
    curl -LO https://github.com/quietvoid/hdr10plus_tool/releases/download/1.3.2/hdr10plus_tool-1.3.2-x86_64-unknown-linux-musl.tar.gz
    tar -xzvf hdr10plus_tool-1.3.2-x86_64-unknown-linux-musl.tar.gz
    cp hdr10plus_tool ~/.local/bin

else
    printf "\n    Must be your user account not root!\n\n"
    exit 1
fi

printf "\n    Success!\n\n"
