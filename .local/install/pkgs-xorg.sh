
#!/bin/env bash
# Location: $HOME/.local/install/pkgs-xorg.sh
# Dependency:
# Description: install xorg packages.
# Usage: sudo pkgs-xorg.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n    Installing xorg packages.\n\n"
    apt --yes install xorg xinput xserver-xorg-legacy twm xfonts-cyrillic
    printf "needs_root_rights=yes" >> /etc/X11/Xwrapper.config
fi 
