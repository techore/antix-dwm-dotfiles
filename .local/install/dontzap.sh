#!/bin/env bash
# Location: $HOME/.local/install/dontzap.sh
# Dependency:
# Description: Create xorg configuration file to prevent killing xorg; slock.
# Usage: sudo dontzap.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

tee /etc/X11/xorg.conf.d/dontzap.conf <<EOF
# Disable ctrl+alt+delete to kill xorg.
Section "ServerFlags"
  Option "DontZap"  "On"
EndSection
EOF
