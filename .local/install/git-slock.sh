#!/usr/bin/env bash
# Location: $HOME/.local/install/slock.sh
# Dependencies: build-essentials (this needs work) 
# Description: suckless screen lock (slock)

if [[ $EUID -gt 0 ]]; then
    mkdir -p ~/code
    cd ~/code
    git clone https://github.com/bakkeby/slock-flexipatch
    cd slock-flexipatch
    make
else
    printf "\n    Must be your user account not root!\n\n"
    exit 1
fi

printf "\n    Success!\n\n"
