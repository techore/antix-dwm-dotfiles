#!/bin/env bash
# Location: $HOME/.local/install/sudoers_nopwd.sh
# Dependency:
# Description: creates dwmers in /etc/sudoers.d to permit use of commands with sudo without password prompt,
#   e.g. apt and apt-get
# Usage: sudo sudoers_nopwd.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

echo "%users ALL=(root) NOPASSWD: /usr/bin/apt" > /etc/sudoers.d/dwmers
echo "%users ALL=(root) NOPASSWD: /usr/bin/apt-gen" >> /etc/sudoers.d/dwmers
chmod 440 /etc/sudoers.d/dwmers
