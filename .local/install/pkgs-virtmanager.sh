#!/bin/env bash
# Location: $HOME/.local/install/pkgs-virtmanager.sh
# Dependency:
# Description: install virt-managers packages.
# Usage: sudo pkgs-virtmanager.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n    Installing virt-manager packages.\n\n"
    apt --yes qemu qemu-kvm libvirt-daemon-system libvirt-clients \
        bridge-utils qemu-utils gir1.2-spiceclientgtk-3.0 virt-manager \
fi 
