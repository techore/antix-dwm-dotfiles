
#!/bin/env bash
# Location: $HOME/.local/install/pkgs-statusbar.sh
# Dependency:
# Description: install package dependences for dwmblocks/statuscmd scripts.
# Usage: sudo pkgs-statusbar.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    apt --yes install dunst libnotify-bin synaptic lxpolkit libpolkit-agent-1-0 curl jq util-linux \
      alsa-tools alsa-utils brightnessctl libyajl-dev
fi
exit
