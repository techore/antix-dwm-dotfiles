#!/bin/env bash
# Location: $HOME/.local/install/pkgs-slock.sh
# Dependency:
# Description: install slock package dependencies.
# Usage: sudo pkgs-slock.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n    Installing slock package dependencies.\n\n"
    apt --yes install xss-lock cmatrix
fi 
