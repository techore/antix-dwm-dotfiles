#!/bin/env bash
# Location: $HOME/.local/install/pkgs-systools.sh
# Dependency:
# Description: install system tool packages.
#     lm-sensors for ixni sys temps and fan speeds
#     nfs-common (mount.nfs) for nfs exports
#     cifs-utils (mount.cifs) for smb shares
# Usage: sudo pkgs-systools.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n    Installing system tool packages.\n\n"
    apt --yes install acpid gparted gpart nemo nemo-fileroller p7zip-full \
        keepassxc gnome-keyring gnome-keyring-pkcs11 gtkhash screenshot-antix \
        snapper snapper-gui python3-dbus lxappearance qemu qemu-kvm \
        libvirt-daemon-system libvirt-clients bridge-utils qemu-utils \
        gir1.2-spiceclientgtk-3.0 virt-manager arandr lm-sensors tmux nfs-common \
        cifs-utils runit-service-manager gksu iso-snapshot-antix \
        live-usb-gui-maker-antix iotop-c
fi 
