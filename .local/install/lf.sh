#!/bin/env bash
# Location: $HOME/.local/install/lf.sh
# Dependency:
# Description: Install lf file manager binary to ~/.local/bin/
# Usage: lf.sh

if [[ $EUID -gt 0 ]]; then
    rm ~/.local/bin/lf
    curl -L https://github.com/gokcehan/lf/releases/latest/download/lf-linux-amd64.tar.gz -o ~/.local/bin/lf
    chmod +x ~/.local/bin/lf
else
    printf "\n    Must be your user account not root!\n\n"
    exit 1
fi

printf "\n    Success!\n\n"
