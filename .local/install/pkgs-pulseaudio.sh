#!/bin/env bash
# Location: $HOME/.local/install/pkgs-pulseaudio.sh
# Dependency:
# Description: install pulseaudio packages.
# Usage: sudo pkgs-pulseaudio.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n    Installing pulseaudio packages.\n\n"
    apt --yes install pulseaudio pulseaudio-utils pavucontrol
fi 
