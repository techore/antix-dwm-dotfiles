#!/bin/env bash
# Location: $HOME/.local/install/pkgs-rofi_conky.sh
# Dependency:
# Description: install rofi and conkey packages.
# Usage: sudo pkgs-rofi_conky.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    apt -y install rofi conky
fi
exit
