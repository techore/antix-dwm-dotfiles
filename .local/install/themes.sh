#!/bin/env bash
# Location: $HOME/.local/install/themes.sh
# Dependency:
# Description: Install GTK themes from github repository.
# Usage: sudo themes.sh

if [[ $EUID -gt 0 ]]; then
    sudo apt install lxappearance qt5ct qt5-style-kvantum
    sudo git -C /usr/share/themes clone https://github.com/EliverLara/Nordic.git
    git -C ~/code clone https://github.com/alvatip/Nordzy-icon.git 
    cd ~/code/Nordzy-icon
    sudo ./install.sh
    git -C ~/code clone https://github.com/alvatip/Nordzy-cursors.git
    cd ~/code/Nordzy-cursors
    sudo ./install.sh
else
    printf "\n    Must be your user account not root!\n\n"
    exit 1
fi
