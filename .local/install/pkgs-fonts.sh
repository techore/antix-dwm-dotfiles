#!/bin/env bash
# Location: $HOME/.local/install/pkgs-fonts.sh
# Dependency:
# Description: install packages that are font install script dependencies.
# Usage: sudo pkgs-fonts.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n    Installing packages that are font install script dependencies.\n\n"
    apt --yes install fontconfig unzip
fi 
