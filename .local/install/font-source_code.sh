#!/bin/env bash
# Location: $HOME/.local/install/font-source_code.sh
# Dependency: curl
# Description: download and copy Adobe Source Code font.
# Usage: sudo font-source_code.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

mkdir /usr/local/share/fonts/sourcecode
cd /usr/local/share/fonts/sourcecode

curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-Black.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-BlackIt.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-Bold.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-BoldIt.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-ExtraLight.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-ExtraLightIt.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-It.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-Light.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-LightIt.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-Medium.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-MediumIt.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-Regular.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-Semibold.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-SemiboldIt.otf 

fc-cache -fv
