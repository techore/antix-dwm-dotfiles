#!/bin/env bash
# Location: $HOME/.local/install/font-source_serif.sh
# Dependency: curl
# Description: download and copy Adobe Source Serif font.
# Usage: sudo font-source_serif.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

mkdir /usr/local/share/fonts/sourceserif
cd /usr/local/share/fonts/sourceserif

curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4-Black.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4-BlackIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4-Bold.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4-BoldIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4-ExtraLight.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4-ExtraLightIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4-It.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4-Light.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4-LightIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4-Regular.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4-Semibold.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4-SemiboldIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Caption-Black.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Caption-BlackIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Caption-Bold.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Caption-BoldIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Caption-ExtraLight.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Caption-ExtraLightIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Caption-It.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Caption-Light.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Caption-LightIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Caption-Regular.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Caption-Semibold.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Caption-SemiboldIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Display-Black.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Display-BlackIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Display-Bold.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Display-BoldIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Display-ExtraLight.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Display-ExtraLightIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Display-It.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Display-Light.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Display-LightIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Display-Regular.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Display-Semibold.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Display-SemiboldIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4SmText-Black.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4SmText-BlackIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4SmText-Bold.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4SmText-BoldIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4SmText-ExtraLight.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4SmText-ExtraLightIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4SmText-It.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4SmText-Light.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4SmText-LightIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4SmText-Regular.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4SmText-Semibold.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4SmText-SemiboldIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Subhead-Black.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Subhead-BlackIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Subhead-Bold.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Subhead-BoldIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Subhead-ExtraLight.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Subhead-ExtraLightIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Subhead-It.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Subhead-Light.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Subhead-LightIt.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Subhead-Regular.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Subhead-Semibold.otf
curl -LO https://github.com/adobe-fonts/source-serif/raw/release/OTF/SourceSerif4Subhead-SemiboldIt.otf 

fc-cache -fv
