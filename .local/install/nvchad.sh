#!/bin/env bash
# Location: $HOME/.local/install/nvchad.sh
# Dependency:
# Description: Install nvchad from github repository.
# Usage: sudo nvchad.sh

if [[ $EUID -gt 0 ]]; then
    sudo apt install ripgrep
    git clone https://github.com/NvChad/NvChad /tmp/nvchad
    cp -r /tmp/nvchad/* ~/.config/nvim
    rm -fr /tmp/nvchad
else
    printf "\n    Must be your user account not root!\n\n"
    exit 1
fi

printf "\n    Success!\n\n"
