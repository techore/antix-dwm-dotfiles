#!/bin/env bash
# Location: $HOME/.local/install/pkgs-misc.sh
# Dependency:
# Description: install system tool packages.
# Usage: sudo pkgs-misc.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n    Installing miscellaneous packages.\n\n"
    apt --yes install gnome-mahjongg
fi 
