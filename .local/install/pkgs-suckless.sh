#!/bin/env bash
# Location: $HOME/.local/install/pkgs-suckless.sh
# Dependency:
# Description: install suckless build dependencies.
# Usage: sudo pkgs-suckless.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n    Installing suckless build dependencies.\n\n"
    apt --yes install build-essential libx11-dev libxft-dev libxinerama-dev libxrandr-dev git 
fi 
