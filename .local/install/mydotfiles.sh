#!/bin/env bash
# Location: $HOME/.local/install/dotfiles.sh
# Dependency:
# Description: install system tool packages.
# Usage: $ dotfiles.sh

if [[ $EUID -gt 0 ]]; then
    sudo apt install git
    mkdir -p ~/code
    cd ~/code
    git clone git@gitlab.com:techore/dotfiles.git
    cd dotfiles
    cp .gtkrc-2.0 ~/
    cp .xinitrc ~/
    cp .Xresources ~/
    cp -r .local ~/
    cp -r .config ~/
else
    printf "\n    Must be your user account not root!\n\n"
    exit 1
fi

printf "\n    Success!\n\n"
