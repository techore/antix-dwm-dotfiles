#!/usr/bin/env bash
# Location: $HOME/.local/install/git-dwmblocks.sh
# Depedencies: build-essential libx11-dev libxft-dev git
# Description: mouse support for executing scripts from statusbar

if [[ $EUID -gt 0 ]]; then
    mkdir -p ~/code
    cd ~/code
    git clone https://github.com/torrinfail/dwmblocks.git
    cd dwmblocks
    curl -O https://dwm.suckless.org/patches/statuscmd/dwmblocks-statuscmd-20210402-96cbb45.diff
    patch -p1 < dwmblocks-statuscmd-20210402-96cbb45.diff
    make
else
    printf "\n    Must be your user account not root!\n\n"
    exit 1
fi

printf "\n    Success!\n\n"
