#!/bin/env bash
# Location: $HOME/.local/install/pkgs-sound.sh
# Dependency:
# Description: install sound packages.
# Usage: sudo pkgs-sound.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n    Installing sound packages.\n\n"
    apt --yes install antix-goodies alsamixer-equalizer-antix libasound2-plugin-equal
fi 
