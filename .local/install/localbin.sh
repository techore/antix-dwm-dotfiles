#!/bin/env bash
# Location: /etc/profile.d/localbin.sh
# Dependency:
# Description: Add $HOME/.local/bin to $PATH for non-root users.
# Usage: Copy localbin.sh to /etc/profile.d/.

if [[ $UID -ge 1000 && -d $HOME/.local/bin && -z $(echo $PATH | grep -o $HOME/.local/bin) ]]
then
    export PATH="${PATH}:$HOME/.local/bin"
fi
