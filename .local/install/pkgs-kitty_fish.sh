#!/bin/env bash
# Location: $HOME/.local/install/pkgs-kitty_fish.sh
# Dependency:
# Description: install kitty and fish packages.
# Usage: sudo pkgs-kitty_fish.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    apt -y install kitty fish
    update-alternatives --config x-terminal-emulator
fi
exit
