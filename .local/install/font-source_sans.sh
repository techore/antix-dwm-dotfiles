#!/bin/env bash
# Location: $HOME/.local/install/font-source_sans.sh
# Dependency: curl
# Description: download and copy Adobe Source Sans font.
# Usage: sudo font-source_sans.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

mkdir /usr/local/share/fonts/sourcesans
cd /usr/local/share/fonts/sourcesans

curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-Black.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-BlackIt.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-Bold.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-BoldIt.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-ExtraLight.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-ExtraLightIt.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-It.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-Light.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-LightIt.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-Regular.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-Semibold.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-SemiboldIt.otf 

fc-cache -fv
