#!/usr/bin/env bash
# Location: $HOME/.local/install/git-dwm.sh
# Dependencies: build-essentials libx11-dev libxft-dev libxinerama-dev git
# Description: suckless dynamic window manager (dwm)

if [[ $EUID -gt 0 ]]; then
    mkdir -p ~/code
    cd ~/code
    git clone https://github.com/bakkeby/dwm-flexipatch
    cd dwm-flexipatch
    make
else
    printf "\n    Must be your user account not root!\n\n"
    exit 1
fi

printf "\n    Success!\n\n"
