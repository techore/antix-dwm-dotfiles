#!/usr/bin/env bash
# Location: $HOME/.local/install/font-nerd_fonts.sh
# Dependencies: wget, unzip, rename
# Description: download and copy Nerd fonts.
# Usage: sudo font-nerd_fonts.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

declare -a fonts=(
    NerdFontsSymbolsOnly
)

version='2.2.2'
fonts_dir="/usr/local/share/fonts/nerdfonts"

if [[ ! -d "$fonts_dir" ]]; then
    mkdir -p "$fonts_dir"
fi

for font in "${fonts[@]}"; do
    zip_file="${font}.zip"
    download_url="https://github.com/ryanoasis/nerd-fonts/releases/download/v${version}/${zip_file}"
    echo "Downloading $download_url"
    curl -LO "$download_url"
    unzip "$zip_file" -d "$fonts_dir"
    rm "$zip_file"
done

find "$fonts_dir" -name '*Windows Compatible*' -delete

fc-cache -fv
