#!/usr/bin/env bash
# Location: $HOME/.local/install/git-dotfiles.sh
# Depedencies: git
# Description: my dot files. 
# Usage: git-dotfiles.sh

if [[ $EUID -gt 0 ]]; then
    mkdir -p ~/code
    cd ~/code
    git clone https://gitlab.com/techore/dotfiles.git
    cd dotfiles
else
    printf "\n    Must be your user account not root!\n\n"
    exit 1
fi

printf "\n    Success!\n\n"
