#!/bin/env bash
# Location: $HOME/.local/install/1440p.sh
# Dependency:
# Description: update respin installation files for 1440p support
# Usage: 1440p.sh

if [[ $EUID -gt 0 ]]; then

    # .Xresources
    sed -i 's/Xft.dpi: 96/Xft.dpi: 128/' ~/.Xresources

    # confdwm.sh
    sed -i '{
        s/static const unsigned int borderpx       = 4/static const unsigned int borderpx       = 6/
        s/static const unsigned int gappih         = 10/static const unsigned int gappih         = 14/
        s/static const unsigned int gappiv         = 10/static const unsigned int gappiv         = 14/
        s/static const unsigned int gappoh         = 10/static const unsigned int gappoh         = 14/
        s/static const unsigned int gappiv         = 99/static const unsigned int gappiv         = 10/
        s/static const unsigned int gappoh         = 99/static const unsigned int gappoh         = 10/
        s/static const unsigned int gappov         = 10/static const unsigned int gappov         = 14/
        s/Material Icons Outlined:pixelsize=16/Material Icons Outlined:pixelsize=22/
    }' ~/.local/bin/confdwm.sh

    # nord.rasi
    sed -i 's/font: "Source Code Pro Regular 12/font: "Source Code Pro Regular 16/' ~/.config/rofi/nord.rasi
    sed -i 's/border:       4/border:       6/' ~/.config/rofi/nord.rasi

    # power.rasi
    sed -i 's/font: "Source Code Pro Regular 18/font: "Source Code Pro Regular 22/' ~/.config/rofi/power.rasi

    # apps.rasi
    sed -i 's/font: "Source Code Pro Regular 18/font: "Source Code Pro Regular 22/' ~/.config/rofi/apps.rasi

    # conky.conf
	  sed -i '{
        s/gap_x = 20/gap_x = 28/
	      s/gap_y = 30/gap_y = 44/
	      s/minimum_width = 350/minimum_width = 460/
    }' ~/.config/conky/conky.conf

else
    printf "\n    Must be your user account not root!\n\n"
    exit 1
fi

printf "\n    Success!\n\n"
