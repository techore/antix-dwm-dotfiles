
#!/bin/env bash
# Location: $HOME/.local/install/pkgs-nvidia.sh
# Dependency:
# Description: install system tool packages.
# Usage: sudo pkgs-av.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n    Installing nVidia packages.\n\n"
    apt --yes install nvidia-driver libnvidia-encode1 libcuda1
fi 
