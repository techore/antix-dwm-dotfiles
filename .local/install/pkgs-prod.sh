#!/bin/env bash
# Location: $HOME/.local/install/pkgs-prod.sh
# Dependency:
# Description: install system tool packages.
# Usage: sudo pkgs-prod.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n    Installing productivity packages.\n\n"
    apt --yes install galculator gsimplecal libreoffice-writer atril leafpad 
fi 
