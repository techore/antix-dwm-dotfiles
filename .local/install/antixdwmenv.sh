#!/bin/env bash
# Location: /etc/profile.d/antixdwmenv.sh
# Dependency:
# Description: Set bash environment variables which are also used by fenv for fish.
# Usage: Copy antixdwmenv.sh to /etc/profile.d/

export TERMINAL="kitty"
export EDITOR="nvim"
export VISUAL="nvim"
export GDK_SCALE="1"
export GDK_DPI_SCALE="1.0"
export QT_AUTO_SCREEN_SCALE_FACTOR="1"
export SU_TO_ROOT_X="gksudo"
