#!/bin/env bash
# Location: $HOME/.local/install/font-material_design.sh
# Dependency: curl
# Description: download and copy Google's Material Design Icons.
# Usage: sudo font-material_design.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

mkdir /usr/local/share/fonts/materialdesign
cd /usr/local/share/fonts//materialdesign

curl -LO https://github.com/google/material-design-icons/raw/master/font/MaterialIcons-Regular.ttf
curl -LO https://github.com/google/material-design-icons/raw/master/font/MaterialIconsOutlined-Regular.otf
curl -LO https://github.com/google/material-design-icons/raw/master/font/MaterialIconsRound-Regular.otf
curl -LO https://github.com/google/material-design-icons/raw/master/font/MaterialIconsSharp-Regular.otf
curl -LO https://github.com/google/material-design-icons/raw/master/font/MaterialIconsTwoTone-Regular.otf

fc-cache -fv
