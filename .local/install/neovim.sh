#!/bin/env bash
# Location: $HOME/.local/install/neovim.sh
# Dependency:
# Description: Install neovim from github repository.
# Usage: sudo neovim.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    apt -y remove --purge neovim
    apt  autoremove
    cd /tmp
    curl -LO https://github.com/neovim/neovim/releases/download/v0.7.2/nvim-linux64.deb
    dpkg -i nvim-linux64.deb
#    update-alternatives --config editor
fi
