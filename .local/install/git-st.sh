#!/usr/bin/env bash
# Location: $HOME/.local/install/git-st.sh
# Dependencies: build-essential libx11-dev libxft-dev git
# Descripton: suckless terminal (st)

if [[ $EUID -gt 0 ]]; then
    mkdir -p ~/code
    cd ~/code
    git clone https://github.com/bakkeby/st-flexipatch
    cd st-flexipatch
    make
else
    printf "\n    Must be your user account not root!\n\n"
    exit 1
fi

printf "\n    Success!\n\n"
