#!/bin/env bash
# Location: $HOME/.local/install/pkgs-av.sh
# Dependency:
# Description: install system tool packages.
# Usage: sudo pkgs-av.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n    Installing audio/video packages.\n\n"
    apt --yes install gpick mirage gimp kodi kodi-repository-kodi mediainfo cmus  \
        cmus-plugin-ffmpeg mpv celluloid ffmpeg ytfzf yt-dlp alsamixer-equalizer-antix \
        libasound2-plugin-equal
    apt --yes --install-recommends install inkscape
fi 
