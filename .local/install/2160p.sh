#!/bin/env bash
# Location: $HOME/.local/install/1440p.sh
# Dependency:
# Description: update respin installation files for 1440p support
# Usage: 1440p.sh

if [[ $EUID -gt 0 ]]; then

    # .Xresources
    sed -i 's/Xft.dpi: 96/Xft.dpi: 192/' ~/.Xresources

    # antixdwmenv.sh
    sed -i '{
        s/export GDK_SCALE="1"/export GDK_SCALE="2"/
        s/export GDK_DPI_SCALE="1.0"/export GDK_DPI_SCALE="0.5"/
    }'

    # confdwm.sh
    sed -i '{
        s/static const unsigned int borderpx       = 4/static const unsigned int borderpx       = 8/
        s/static const unsigned int gappih         = 10/static const unsigned int gappih         = 20/
        s/static const unsigned int gappiv         = 10/static const unsigned int gappiv         = 20/
        s/static const unsigned int gappoh         = 10/static const unsigned int gappoh         = 20/
        s/static const unsigned int gappiv         = 99/static const unsigned int gappiv         = 10/
        s/static const unsigned int gappoh         = 99/static const unsigned int gappoh         = 10/
        s/static const unsigned int gappov         = 10/static const unsigned int gappov         = 20/
        s/Material Icons Outlined:pixelsize=16/Material Icons Outlined:pixelsize=32/
    }' ~/.local/bin/confdwm.sh

    # nord.rasi
    sed -i 's/font: "Source Code Pro Regular 12/font: "Source Code Pro Regular 24/' ~/.config/rofi/nord.rasi
    sed -i 's/border:       4/border:       8/' ~/.config/rofi/nord.rasi

    # power.rasi
    sed -i 's/font: "Source Code Pro Regular 18/font: "Source Code Pro Regular 36/' ~/.config/rofi/power.rasi

    # apps.rasi
    sed -i 's/font: "Source Code Pro Regular 18/font: "Source Code Pro Regular 36/' ~/.config/rofi/apps.rasi

    # conky.conf
	  sed -i '{
        s/gap_x = 20/gap_x = 40/
	      s/gap_y = 30/gap_y = 60/
	      s/minimum_width = 350/minimum_width = 700/
    }' ~/.config/conky/conky.conf

else
    printf "\n    Must be your user account not root!\n\n"
    exit 1
fi

printf "\n    Success!\n\n"
