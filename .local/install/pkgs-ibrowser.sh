#!/bin/env bash
# Location: $HOME/.local/install/pkgs-ibrowser.sh
# Dependency:
# Description: install Internet browser(s).
# Usage: sudo pkgs-ibrowser.sh

if [[ $EUID -ne 0 ]]; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n    Installing Internet browser package.\n\n"
    apt --yes install firefox-esr qutebrowser
fi 
