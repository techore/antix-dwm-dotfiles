
#!/bin/env bash
# Location: $HOME/.local/install/clean.sh
# Dependency:
# Description: remove default files and directories
# Usage: clean.sh

if [[ $EUID -gt 0 ]]; then
    cd ~
    rm .bash_logout
    rm .bashrc
    rm .conkyrc
    rm .conkyrc-lua
    rm -fr .fluxbox
    rm -fr .icewm
    rm -fr .jwm
    rm .nanorc
    rm .profile
    rm -fr .config
    rm -fr .local
else
    printf "\n    Must be your user account not root!\n\n"
    exit 1
fi

printf "\n    Success!\n\n"
