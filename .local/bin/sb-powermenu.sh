#!/usr/bin/env bash
# Location: $HOME/.local/bin/sb-powermenu.sh
# Dependencies: powermenu
# Description: click to execute ~/.local/bin/powermenu
# Usage: include in dwmblocks/blocks.h. 
#   Trigger is mouse buttons left (1), middle (2), and right (3) where
#   button+shift for 4, 5, and 6.

powerm () {

    case $BUTTON in
        1) powermenu ;;
#        2) ;;
#        3) ;;
#        4) ;;
#        5) ;;
        6) "$TERMINAL" -e "$EDITOR" "$0" ;;
    esac

    printf " "

}

powerm
