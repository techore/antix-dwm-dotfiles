#!/bin/env bash
# Location: $HOME/.local/bin/confst.sh
# Dependencies:
# Description: To avoid maintaining and reconciling a fork, this script updates
# configuration files then compiles and installs binaries.
# Usage: `confst.sh path` where path is the directory for patches.def.h and
# config.def.h.

# Was a parameter provided?
if test -z $1; then
    printf "\n    Missing parameter!"
    printf "\n    Syntax: confdwm.sh ~/code/st-flexipatch/\n\n" 
    exit 1
fi

confdir=$1

# Test parameter/directory.
if test ! -d "$confdir"; then
    printf "\n    Directory test failed!"
    printf "\n    Provided the correct path to configuration files.\n\n"
    exit 1
fi

# Verify patches.def.h is present.
if test ! -f "$confdir/patches.def.h"; then
    printf "\n    File test failed!"
    printf "\n    File patches.def.h is missing.\n\n"
    exit 1
fi

# Verify config.def.h is present.
if test ! -f "$confdir/config.def.h"; then
    printf "\n    File test failed!"
    printf "\n    File config.def.h is missing.\n\n"
    exit 1
fi

# Test for root or sudo.
if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

# Tests complete!

# Backup patches.h if exists then copy patches.def.h to patches.h.
if test -f "$confdir/patches.h"; then
    cp "$confdir/patches.h" "$confdir/patches.h.bak"
    cp "$confdir/patches.def.h" "$confdir/patches.h"
    else
        cp "$confdir/patches.def.h" "$confdir/patches.h"
fi

# Backup config.h if exists then copy config.def.h to config.h.
if test -f "$confdir/config.h"; then
    cp "$confdir/config.h" "$confdir/config.h.bak"
    cp "$confdir/config.def.h" "$confdir/config.h"
    else
        cp "$confdir/config.def.h" "$confdir/config.h"
fi

# sed: escape $.*/[\]^
# sed: tab = \t, newline = \n, use of \\ may be necessary

# Configure config.h
sed -i '{
    s/static char *font = "Liberation Mono:pixelsize=12:antialias=true:autohint=true"/static char *font = "Source Code Pro:pixelsize=12:antialias=true:autohint=true"r/
}' "$confdir/config.h"

# Compile and install binaries
make -C "$confdir" clean install

printf "\nSuccess!\n\n"

exit
