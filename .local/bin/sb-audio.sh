#!/usr/bin/env bash
# Location: $HOME/.local/bin/sb-audio.sh
# Dependencies: amixer and alsamixer from alsa-tools and alsa-utils packages
# Description: Display current volume using amixer and launch alsamixer
# Usage: update dwmblocks/blocks.h. 
#   Trigger is mouse buttons left (1), middle (2), and right (3) where
#   button+shift for 4, 5, and 6.

tog=$(amixer -M get Master | tail -n1 | sed -r 's/.*\[(o.*)\].*/\1/')
vol=$(amixer -M get Master | tail -n1 | sed -r 's/.*\[(.*)%\].*/\1/')

audio() {

if [ "$tog" = "off" ]; then
    printf ""
else
    printf " %s%%" "$vol"
fi 

}

audio
