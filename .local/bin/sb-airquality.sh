#!/usr/bin/env bash
# Location: $HOME/.local/bin/sb-airquality.sh
# Dependencies:
#  curl, jq, xdg-open, and Internet browser
#  Download and print air quality info from aqicn.org to statusbar.
#  Obtain $APPID from https://aqicn.org/data-platform/token/
#  Obtain $LOCATION from https://aqicn.org/nearest
# Usage: update dwmblocks/blocks.h. 
#   Trigger is mouse buttons left (1), middle (2), and right (3) where
#   button+shift for 4, 5, and 6.

APPID=""
LOCATION="McAlester"
APIURL="https://api.waqi.info/feed"
PAGEURL="https://aqicn.org/city/usa/oklahoma/mcalester/"

aqi=$(curl -sf "$APIURL/$LOCATION/?token=$APPID" | jq '.data.aqi')

airquality () {

    case $BUTTON in
	1) notify-send "Air Quality Index: $aqi" "50: Good\n100: Moderate\n150: Unhealthy for Sensitives\n200: Unhealthy\n300: Very Unhealthy\n500: Hazardous" ;;
#        2) ;;
        3) xdg-open "$PAGEURL" ;;
#        4) ;;
#        5) ;;
        6) "$TERMINAL" -e "$EDITOR" "$0" ;;
    esac

    printf " %s" "$aqi"

}

airquality
