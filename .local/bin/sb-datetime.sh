#!/usr/bin/env bash
# Location: $HOME/.local/bin/sb-datetime.sh
# Dependencies: Material Design Icons
# Description: print to dwmblocks current day of week, date and hour:minute.
# Usage: update dwmblocks/blocks.h. 
#   Trigger is mouse buttons left (1), middle (2), and right (3) where
#   button+shift for 4, 5, and 6.

datetime () {

case $BUTTON in
    1) notify-send "$(date)" ;;
#    2) ;;
    3) notify-send "$(cal)" ;;
#    4) notify-send "$(calcurse -a)" ;;
#    5) ;;
    6) "$TERMINAL" -e "$EDITOR" "$0" ;;
esac

    printf " %s" "$(date "+%A %d | %H:%M")"
}

datetime
