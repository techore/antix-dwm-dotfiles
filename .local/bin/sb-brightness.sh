#!/usr/bin/env bash
# Location: $HOME/.local/bin/sb-brightness.sh
# Dependencies: brightnessctl package and account is a member of video group
# Descriptiion: display the current backlight brightness percent. Also,
# this script will be called by dwm-brightness.sh everytime XF86MonBrightnessDown
# and XF86MonBrightnessUp is detected to update dwmblocks.

current=$(brightnessctl g)
maximum=$(brightnessctl m)
ratio=$(bc <<< "scale=3; $current/$maximum")
percent=$(bc <<< "$ratio*100")
round=$(printf "%.0f" "$percent")
printf " $round%%"
