#!/bin/env bash
# Location: $HOME/.local/bin/confdwm.sh
# Dependencies:
# Description: To avoid maintaining and reconciling a fork, this script updates
# configuration files then compiles and installs binaries.
# Usage: `confdwm.sh path` where path is the directory for patches.def.h and
# config.def.h.

# Was a parameter provided?
if test -z $1; then
    printf "\n    Missing parameter!"
    printf "\n    Syntax: confdwm.sh ~/code/dwm-flexipatch/\n\n" 
    exit 1
fi

confdir=$1

# Test parameter/directory.
if test ! -d "$confdir"; then
    printf "\n    Directory test failed!"
    printf "\n    Provided the correct path to configuration files.\n\n"
    exit 1
fi

# Verify config.mk is present.
if test ! -f "$confdir/config.mk"; then
    printf "\n    File test failed!"
    printf "\n    File config.mk is missing.\n\n"
    exit 1
fi

# Verify patches.def.h is present.
if test ! -f "$confdir/patches.def.h"; then
    printf "\n    File test failed!"
    printf "\n    File patches.def.h is missing.\n\n"
    exit 1
fi

# Verify config.def.h is present.
if test ! -f "$confdir/config.def.h"; then
    printf "\n    File test failed!"
    printf "\n    File config.def.h is missing.\n\n"
    exit 1
fi

# Test for root or sudo.
if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

# Tests complete!

# Backup config.mk if exists.
if test -f "$confdir/config.mk"; then
    cp "$confdir/config.mk" "$confdir/config.mk.bak"
fi

# Backup config.h if exists then copy config.def.h to config.h.
if test -f "$confdir/config.h"; then
    cp "$confdir/config.h" "$confdir/config.h.bak"
    cp "$confdir/config.def.h" "$confdir/config.h"
    else
        cp "$confdir/config.def.h" "$confdir/config.h"
fi

# sed: escape $.*/[\]^
# sed: tab = \t, newline = \n, use of \\ may be necessary

# Configure config.mk
sed -i '{
    s/#YAJLLIBS = -lyajl/YAJLLIBS = -lyajl/
    s/#YAJLINC = -I\/usr\/include\/yajl/YAJLINC = -I\/usr\/include\/yajl/
}' "$confdir/config.mk"

# Configure patches.h
sed -i '{
    s/#define BAR_AWESOMEBAR_PATCH 0/#define BAR_AWESOMEBAR_PATCH 1/
    s/#define COOL_AUTOSTART_PATCH 0/#define COOL_AUTOSTART_PATCH 1/
    s/#define IPC_PATCH 0/#define IPC_PATCH 1/
    s/#define CYCLELAYOUTS_PATCH 0/#define CYCLELAYOUTS_PATCH 1/
    s/#define BAR_DWMBLOCKS_PATCH 0/#define BAR_DWMBLOCKS_PATCH 1/
    s/#define ALWAYSCENTER_PATCH 0/#define ALWAYSCENTER_PATCH 1/
    s/#define ATTACHBELOW_PATCH 0/#define ATTACHBELOW_PATCH 1/
    s/#define MOVESTACK_PATCH 0/#define MOVESTACK_PATCH 1/
    s/#define NOBORDER_PATCH 0/#define NOBORDER_PATCH 1/
    s/#define RESTARTSIG_PATCH 0/#define RESTARTSIG_PATCH 1/
    s/#define RENAMED_SCRATCHPADS_PATCH 0/#define RENAMED_SCRATCHPADS_PATCH 1/
    s/#define RENAMED_SCRATCHPADS_AUTO_HIDE_PATCH 0/#define RENAMED_SCRATCHPADS_AUTO_HIDE_PATCH 1/
    s/#define BAR_STATUSCMD_PATCH 0/#define BAR_STATUSCMD_PATCH 1/
    s/#define VANITYGAPS_PATCH 0/#define VANITYGAPS_PATCH 1/
    s/#define FOCUSONCLICK_PATCH 0/#define FOCUSONCLICK_PATCH 1/
    s/#define TOGGLEFULLSCREEN_PATCH 0/#define TOGGLEFULLSCREEN_PATCH 1/
    s/#define LOSEFULLSCREEN_PATCH 0/#define LOSEFULLSCREEN_PATCH 1/
}' "$confdir/patches.h"

# Configure config.h
sed -i '{
    s/#define MODKEY Mod1Mask/#define MODKEY Mod4Mask/
    s/static const char \*termcmd\[\]  = { "st", NULL }/static const char \*termcmd\[\]  = { "kitty", NULL }/
    s/static const unsigned int borderpx       = 1/static const unsigned int borderpx       = 4/
    s/static const unsigned int gappih         = 20/static const unsigned int gappih         = 10/
    s/static const unsigned int gappiv         = 99/static const unsigned int gappiv         = 10/
    s/static const unsigned int gappoh         = 99/static const unsigned int gappoh         = 10/
    s/static const unsigned int gappov         = 30/static const unsigned int gappov         = 10/
    s/static const int smartgaps_fact          = 1/static const int smartgaps_fact          = 0/
    s/static const char \*fonts\[\]               = { "monospace:size=10" }/static const char \*fonts\[\]               = { "Source Sans 3:size=12", "Material Icons Outlined:pixelsize=16:antialias=true:autohint=true" }/
    s/static const char \*scratchpadcmd\[\] = {"s", "st", "-n", "spterm", NULL}/static const char \*scratchpadcmd\[\] = {"s", "kitty", "--class", "spterm", NULL}/
    s/const char \*spcmd1\[\] = {"st", "-n", "spterm", "-g", "120x34", NULL };/const char \*spcmd1\[\] = {"kitty", "--class", "spterm", "--title", "scratchpad", NULL };/
    s/RULE(.instance = "spterm", .tags = SPTAG(0), .isfloating = 1)/RULE(.class = "spterm", .tags = SPTAG(0), .isfloating = 1)/
    s/	{ MODKEY|Mod4Mask,              XK_0,          togglegaps,             {0} },/	{ MODKEY|Mod4Mask,              XK_g,          togglegaps,             {0} },/
    s/	{ MODKEY|Mod4Mask|ShiftMask,    XK_0,          defaultgaps,            {0} },/ 	{ MODKEY|Mod4Mask|ShiftMask,    XK_g,          defaultgaps,            {0} },/
    s/static char normfgcolor\[\]                = "#bbbbbb"/static char normfgcolor\[\]                = "#d8dee9"/
    s/static char normbgcolor\[\]                = "#222222"/static char normbgcolor\[\]                = "#2e3440"/
    s/static char normbordercolor\[\]            = "#444444"/static char normbordercolor\[\]            = "#4c566a"/
    s/static char normfloatcolor\[\]             = "#db8fd9"/static char normfloatcolor\[\]             = "#4c566a"/
    s/static char selfgcolor\[\]                 = "#eeeeee"/static char selfgcolor\[\]                 = "#2e3440"/
    s/static char selbgcolor\[\]                 = "#005577"/static char selbgcolor\[\]                 = "#ebcb8b"/
    s/static char selbordercolor\[\]             = "#005577"/static char selbordercolor\[\]             = "#5e81ac"/
    s/static char selfloatcolor\[\]              = "#005577"/static char selfloatcolor\[\]              = "#5e81ac"/
    s/static char titlenormfgcolor\[\]           = "#bbbbbb"/static char titlenormfgcolor\[\]           = "#d8deed"/
    s/static char titlenormbgcolor\[\]           = "#222222"/static char titlenormbgcolor\[\]           = "#2e3440"/
    s/static char titlenormbordercolor\[\]       = "#444444"/static char titlenormbordercolor\[\]       = "#2e3440"/
    s/static char titlenormfloatcolor\[\]        = "#db8fd9"/static char titlenormfloatcolor\[\]        = "#2e3440"/
    s/static char titleselfgcolor\[\]            = "#eeeeee"/static char titleselfgcolor\[\]            = "#e5e9f0"/
    s/static char titleselbgcolor\[\]            = "#005577"/static char titleselbgcolor\[\]            = "#5e81ac"/
    s/static char titleselbordercolor\[\]        = "#005577"/static char titleselbordercolor\[\]        = "#5e81ac"/
    s/static char titleselfloatcolor\[\]         = "#005577"/static char titleselfloatcolor\[\]         = "#5e81ac"/
    s/static char tagsnormfgcolor\[\]            = "#bbbbbb"/static char tagsnormfgcolor\[\]            = "#d8deed"/
    s/static char tagsnormbgcolor\[\]            = "#222222"/static char tagsnormbgcolor\[\]            = "#2e3440"/
    s/static char tagsnormbordercolor\[\]        = "#444444"/static char tagsnormbordercolor\[\]        = "#2e3440"/
    s/static char tagsnormfloatcolor\[\]         = "#db8fd9"/static char tagsnormfloatcolor\[\]         = "#2e3440"/
    s/static char tagsselfgcolor\[\]             = "#eeeeee"/static char tagsselfgcolor\[\]             = "#e5e9f0"/
    s/static char tagsselbgcolor\[\]             = "#005577"/static char tagsselbgcolor\[\]             = "#5e81ac"/
    s/static char tagsselbordercolor\[\]         = "#005577"/static char tagsselbordercolor\[\]         = "#5e81ac"/
    s/static char tagsselfloatcolor\[\]          = "#005577"/static char tagsselfloatcolor\[\]          = "#5e81ac"/
    s/static char hidnormfgcolor\[\]             = "#005577"/static char hidnormfgcolor\[\]             = "#5e81ac"/
    s/static char hidselfgcolor\[\]              = "#227799"/static char hidselfgcolor\[\]              = "#81a1c1"/
    s/static char hidnormbgcolor\[\]             = "#222222"/static char hidnormbgcolor\[\]             = "#2e3440"/
    s/static char hidselbgcolor\[\]              = "#222222"/static char hidselbgcolor\[\]              = "#2e3440"/
    s/static char urgfgcolor\[\]                 = "#bbbbbb"/static char urgfgcolor\[\]                 = "#8fbcbb"/
    s/static char urgbgcolor\[\]                 = "#222222"/static char urgbgcolor\[\]                 = "#88c0d0"/
    s/static char urgbordercolor\[\]             = "#ff0000"/static char urgbordercolor\[\]             = "#88c0d0"/
    s/static char urgfloatcolor\[\]              = "#db8fd9"/static char urgfloatcolor\[\]              = "#88c0d0"/
    s/static char scratchselfgcolor\[\]          = "#FFF7D4"/static char scratchselfgcolor\[\]          = "#d8dee9"/
    s/static char scratchselbgcolor\[\]          = "#77547E"/static char scratchselbgcolor\[\]          = "#2e3440"/
    s/static char scratchselbordercolor\[\]      = "#894B9F"/static char scratchselbordercolor\[\]      = "#A3BE8C"/
    s/static char scratchselfloatcolor\[\]       = "#894B9F"/static char scratchselfloatcolor\[\]       = "#A3BE8C"/

    s/static char scratchnormfgcolor\[\]         = "#FFF7D4"/static char scratchnormfgcolor\[\]         = "#d8dee9"/
    s/static char scratchnormbgcolor\[\]         = "#664C67"/static char scratchnormbgcolor\[\]         = "#2e3440"/
    s/static char scratchnormbordercolor\[\]     = "#77547E"/static char scratchnormbordercolor\[\]     = "#8FBCBB"/
    s/static char scratchnormfloatcolor\[\]      = "#77547E"/static char scratchnormfloatcolor\[\]      = "#8FBCBB"/
}' "$confdir/config.h"

## statuscmd patch
sed -i '/{ ClkStatusText,        0,                   Button3,        sigstatusbar,   {\.i = 3 } },/a \\t{ ClkStatusText,        ShiftMask,           Button1,        sigstatusbar,   {\.i = 4 } },\n	{ ClkStatusText,        ShiftMask,           Button2,        sigstatusbar,   {\.i = 5 } },\n	{ ClkStatusText,        ShiftMask,           Button3,        sigstatusbar,   {\.i = 6 } },' "$confdir/config.h"

## dwmblocks and conky
sed -i '/static const char \*const autostart\[\] = {/a \\t"dwmblocks", NULL,\n\t"conky", NULL,' "$confdir/config.h"
sed -i '/"st", NULL,/d' "$confdir/config.h"

## keybindings
sed -i '/static const Key keys/a \\t{ MODKEY,                       XK_a,          spawn,                  SHCMD("appsmenu") },' "$confdir/config.h"
sed -i '/static const char \*termcmd\[\]  = { "kitty", NULL };/a static const char \*filemanager\[\]  = { "kitty", "lf", NULL };' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{ MODKEY|ControlMask,           XK_l,          spawn,                  {.v = filemanager } },' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{ MODKEY,                       XK_a,          spawn,                  SHCMD("appsmenu") },' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{ MODKEY,                       XK_p,          spawn,                  SHCMD("powermenu") },' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{ MODKEY,                       XK_r,          spawn,                  SHCMD("rofi -modi drun#window#run#ssh -show drun -show-icons -dpi 96") },' "$confdir/config.h"

## rules
sed -i '{
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "MakeMKV BETA", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "fastflix", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Gtkhash", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "iso-snapshot", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "live-usb-maker-gui-antix", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Galculator", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "KeePassXC", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Gpick", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "GParted", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Snapper-gui", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "iso-snapshot", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Arandr", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Pavucontrol", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "KvantumViewer", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Kvantum Manager", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "qt5ct", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Lxappearance", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Gtkdialog", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Yad", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Virt-manager", .isfloating = 1)
}' "$confdir/config.h"
# Delete default tag assignments
sed -i '/RULE(.class = "Gimp", .tags = 1 << 4)/d' "$confdir/config.h"
sed -i '/RULE(.class = "Firefox", .tags = 1 << 7)/d' "$confdir/config.h"

# Compile and install binaries
make -C "$confdir" clean install

printf "\nSuccess!\n\n"

exit
