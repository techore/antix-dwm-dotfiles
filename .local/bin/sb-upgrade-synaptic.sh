#!/usr/bin/env bash
# Location: $HOME/.local/bin/sb-upgrade-synaptic.sh
# Dependencies: synaptic, lxpolkit, libpolkit-agent-1-0,
# Description: mouse left click to launch synaptic.
# Usage: this script is called by dwmblocks. 
# Reference: https://github.com/LukeSmithxyz/dwmblocks

# Prompt for root password then execute synaptic
gksudo synaptic

# Update /var/tmp/aptupg.out
sudo -u root bash -c '\
  upgrades=$(apt-get upgrade --simulate --quiet=2 | grep Inst | wc -l)
  printf "$upgrades\n" > /var/tmp/aptupg.out'

# Refresh upgrades on the statusbar
kill -38 $(pidof dwmblocks)
