#!/usr/bin/env bash
# Location: $HOME/.local/bin/dwm-audio.sh
# Dependencies: amixer
# Descriptiion: Incrment and decrement audio volume or mute. In addition,
# this script will kill sb-audio.sh signals to update dwmblocks.

if [ "$1" = "inc" ]; then
        amixer -q -M set Master 1%+ unmute
fi
if [ "$1" = "dec" ]; then
        amixer -q -M set Master 1%- unmute
fi
if [ "$1" = "toggle" ]; then
	amixer -q -M set Master toggle
fi

pkill -RTMIN+6 dwmblocks
