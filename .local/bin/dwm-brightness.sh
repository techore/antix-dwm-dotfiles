#!/usr/bin/env bash
# Location: $HOME/.local/bin/dwm-brightness.sh
# Dependencies: brightnessctl package and account is a member of video group
# Descriptiion: Incrment and decrement backlight brightness. In addition,
# this script will kill sb-brightness.sh signals to update dwmblocks.

if [ "$1" = "inc" ]; then
        brightnessctl -q set +10%
fi
if [ "$1" = "dec" ]; then
        brightnessctl -q set 10%-
fi

pkill -RTMIN+7 dwmblocks
