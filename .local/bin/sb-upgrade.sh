#!/usr/bin/env bash
# Location: $HOME/.local/bin/sb-upgrade.sh
# Dependencies: synaptic, lxpolkit, libpolkit-agent-1-0,
# Description: mouse right click to launch synaptic upgrade mode and mouse right
#   click to launch synaptic.
# Usage: update dwmblocks/blocks.h. 
#   Trigger is mouse buttons left (1), middle (2), and right (3) where
#   button+shift for 4, 5, and 6.

file=/var/tmp/aptupg.out

upgrades () {
  case $BUTTON in
    1) bash -c sb-upgrade-synaptic-mode.sh ;;
#   2) ;;
    3) bash -c sb-upgrade-synaptic.sh ;;
#   4) ;;
#   5) ;;
    6) "$TERMINAL" -e "$EDITOR" "$0" ;;
  esac
}

upgrades

if test -f "$file"; then
  pkgupdates=$(cat $file)
else
  pkgupdates="0"
fi

printf " %s" "$pkgupdates"
