# Archived!

This project has been superceded by https://gitlab.com/techore/antix-dwm.

## antiX DWM dotfiles 

The [antiX DWM Guide](https://gitlab.com/techore/antix-dwm-guide) details how to create a dwm respin using antix-core. Most of the steps are a manual process, but I have created scripts to simplify the installation. This article provides instructions on the use of this git repository's dotfiles and install scripts.

I advise reviewing the antix DWM Guide in its entirety then use this "Install process" to build. Alternativley, you can just follow "Install process" but If errors occur, review the corresponding guide article which are by subject.

Please open an issue to ask for help or recommend improvements.

## Install process

### Complete articles from the antiX DWM Guide:

1. [Install media](https://gitlab.com/techore/antix-dwm-guide/-/blob/main/docs/install_media.md)
2. [Storage](https://gitlab.com/techore/antix-dwm-guide/-/blob/main/docs/storage.md)
3. [antiX install](https://gitlab.com/techore/antix-dwm-guide/-/blob/main/docs/antix_install.md)
4. [Boot manager](https://gitlab.com/techore/antix-dwm-guide/-/blob/main/docs/boot_manager.md)

### Execute

1. Reboot using rEFInd and login as root
2. Set time if needed using `date --set 20220913` (YYYYMMDD) or `date --set hh:mm:ss` and `hwclock --systohc`.
3. Verify connectivity using `ip addr` and if not, `ceni` to setup interface.
4. Delete the contents of user account home directory, `shopt -s dotglob && rm -fr /home/useraccount/*`
5. Exit and login with your user account
6. (Optional: install gpg keys at ~/.ssh for git repositories)
7. `mkdir ~/code && cd code`
8. `git clone https://gitlab.com/techore/antix-dwm-dotfiles.git`
9. copy dotfiles to $HOME, `shopt -s dotglob && cp -R ~/code/antix-dwm-dotfiles/* ~/ && rm -fr ~/.git ~/index.md ~/license.md ~/changelog.md` 
10. `cd ~`
11. (Optional: `.local/install/1440p.sh` or `.local/install/2160p.sh` to update dotfiles for 1440p or 2160p support)
12. `sudo cp .local/install/localbin.sh /etc/profile.d/`
13. `source /etc/profile.d/localbin.sh`
14. `sudo cp .local/install/gksu.sh /etc/profile.d/`
15. `source /etc/profile.d/gksu.sh`
16. `su -`
17. `echo "QT_QPA_PLATFORMTHEME=qt5ct" >> /etc/environment`
18. `exit`
19. `sudo apt update`
20. `sudo .local/install/pkgs-xorg.sh` <--***** verify /etc/X11/Xwrapper.conf, last install had dup and mangled syntax
21. (Optional: update ~/.xinitrc to use twm and startx to test)
22. Install display driver: `sudo ddm-mx -i nvidia` or `sudo apt install xserver-xorg-video-intel` **reboot**, and startx to test
23. Update ~/.xinitrc to use dwm
24. `sudo .local/install/pkgs-fonts.sh`
25. `sudo .local/install/font-material_design.sh`
26. `sudo .local/install/font-nerd_fonts.sh`
27. `sudo .local/install/font-source_code.sh`
28. `sudo .local/install/font-source_sans.sh`
29. `sudo .local/install/font-source_serif.sh`
30. `sudo .local/install/pkgs-suckless.sh`
31. `sudo .local/install/pkgs-slock.sh`
32. `sudo .local/install/pkgs-statusbar.sh`
33. `sudo .local/install/pkgs-rofi_conky.sh`
34. `sudo .local/install/pkgs-sound.sh`
35. (Optional: `.local/install/pkgs-pulseaudio.sh`) <-- I don't use pulseaudio, however, you may need it.
36. `sudo .local/install/dontzap.sh`
37. `.local/install/git-dwm.sh`
38. `.local/install/git-dwmblocks.sh`, the warning regarding unused variables can be ignored
39. `.local/install/git-st.sh`
40. `.local/install/git-slock.sh`
41. `sudo .local/bin/confdwm.sh code/dwm-flexipatch`
42. `sudo .local/bin/confdwmblocks.sh code/dwmblocks`
43. `sudo .local/bin/confst.sh code/st-flexipatch`
44. `sudo .local/bin/confslock.sh code/slock-flexipatch`
45. `sudo .local/install/pkgs-kitty_fish.sh`
46. `chsh -s $(which fish)`
47. `fish` set fish_user_paths variables
48. `set -U fish_user_paths $fish_user_paths /sbin`
49. `set -U fish_user_paths $fish_user_paths /usr/sbin`
50. `set -U fish_user_paths $fish_user_paths $HOME/.local/bin`
51. `exit`
52. `.local/install/nvchad.sh`
53. Execute `nvim` and packer.nvim should trigger and update lua addons
54. `sudo .local/install/pkgs-systools.sh`
55. `sudo .local/install/pkgs-prod.sh`
56. `sudo .local/install/pkgs-av.sh`
57. `sudo .local/install/pkgs-misc.sh`
58. `sudo .local/install/pkgs-ibrowser.sh`
59. `.local/install/lf.sh`
60. `sudo .local/install/makemkv.sh`
61. `.local/install/fastflix.sh`
62. `sudo .local/install/pkgs-virtmanager.sh`
63. `.local/install/themes.sh`
64. For battery and LCD brightness on laptops, `nvim ~/code/dwmblocks/blocks.h` and uncomment sb-battery.sh and sb-brightness.sh then `sudo make clean install`
65. Update ~/.local/bin/sb-airquality.sh with your forty alpha-numberical API key or token ($APPID).
66. Update ~/.local/bin/sb-airquality.sh with your $LOCATION and $PAGEURL.
67. Update ~/.local/bin/sb-weather.sh with your thirty-two alpha-numerical API key or token ($APPID).
68. Update ~/.local/bin/sb-weather.sh wih your $LOCATION.
69. sudo .local/instal/aptupgrade /etc/cron.daily/
70. `sudo rm /etc/udev/rules/d/70-persistent-net.rules` to generate a new one on next boot
71. `sudo reboot`
72. `sudo ceni` delete inherited interface configurations and configure eth0 or appropriate interface
73. `startx`
74. Execute `runit-service-manager.sh` and enable cron and anacron (<-- is enabling anacron really needed?)
75. Done!

**Notes**

`shopt` is bash specific and is used to copy "." aka dotfiles in situations where it will not to so by default.
