# Defined in - @ line 1
function s --wraps='kitty +kitten ssh' --description 'alias s=kitty +kitten ssh'
  kitty +kitten ssh $argv;
end
