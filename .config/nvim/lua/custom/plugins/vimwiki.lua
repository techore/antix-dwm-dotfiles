vim.g.vimwiki_list = {{path = '~/code/antix-dwm-guide/', syntax = 'markdown', ext = '.md'},
    {path = '~/code/antix-dwm-dotfiles/', syntax = 'markdown', ext = '.md'},
    {path = '~/code/cookbook/', syntax = 'markdown', ext = '.md'}}
vim.g.vimwiki_ext2syntax = {['.md'] = 'markdown', ['.markdown'] = 'markdown', ['.mdown'] = 'markdown'}
