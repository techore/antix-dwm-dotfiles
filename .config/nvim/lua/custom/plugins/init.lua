-- example file i.e lua/custom/init.lua
return {

   ["vimwiki/vimwiki"] = {
      config = function()
         require "custom.plugins.vimwiki"
      end,
   },
}
-- load your globals, autocmds here or anything .__.
